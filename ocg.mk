OCG_DIR=conf/${stem}s
.PHONY: ocg-dir
ocg-dir:
	test -n "${OCG_DIR}"												||	(	\
		echo ${indent}Please set OCG_DIR!									&&	\
		exit 1																)
	test -d "${OCG_DIR}"												||	(	\
		mkdir -p ${OCG_DIR}													)

.PHONY: ocg-co
ocg-co: stem=bin
ocg-co: ocg-dir
	echo ${indent}Verifying ${name} in ${target}
	${make} updateRepo branch=HEAD~1 name=${target}								\
			repo=${gerritHead}/mediawiki/extensions/Collection/${name}			\
			stem=${stem}

.PHONY: ocg-build
ocg-build: stem=bin
ocg-build: ocg-dir
	echo ${indent}Building ${name} in ${target}
	cd ${OCG_DIR}/${target} && npm install

OCG_EXT=Collection
# Offline Content Generator
.PHONY: OCG
OCG:
	${make} ocg-co target=mw-ocg-service name=OfflineContentGenerator
	${make} ocg-co target=mw-ocg-bundler name=OfflineContentGenerator/bundler
	${make} ocg-co target=mw-ocg-latexter name=OfflineContentGenerator/latex_renderer
	${make} ocg-co target=mw-ocg-texter name=OfflineContentGenerator/text_renderer
	${make} ocg-build target=mw-ocg-service name=OfflineContentGenerator
	${make} ocg-build target=mw-ocg-bundler name=OfflineContentGenerator/bundler
	${make} ocg-build target=mw-ocg-latexter name=OfflineContentGenerator/latex_renderer
	${make} ocg-build target=mw-ocg-texter name=OfflineContentGenerator/text_renderer
