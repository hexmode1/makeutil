dbHost ?= localhost
prodDBServer ?= localhost
dbName ?= wiki
rootDbUser ?= root
rootDbPass ?= WiKiD3v
dumpOpts := ${dumpOpts} --set-gtid-purged=OFF
dbBackupDir ?= /u01/backup
dbBackupFile ?= ${dbBackupDir}/${MW_DB_NAME}-$(shell date +%Y-%m-%d:%H:%M).sql

dbRename:
	test -n "${oldDBName}"												||	(	\
		echo "Please set oldDBName"											&&	\
		exit 1																)
	test -n "${newDBName}"												||	(	\
		echo "Please set newDBName"											&&	\
		exit 1																)
	mysqladmin -u ${rootDbUser} -p${rootDbPass} create ${newDBName}
	mysql -u ${rootDbUser} -p${rootDbPass} ${oldDBName} -sNe					\
		'show tables'														|	\
		while read table												;	do	\
			mysql -u ${rootDbUser} -p${rootDbPass} -sNe							\
				"RENAME TABLE ${oldDBName}.$$table TO ${newDBName}.$$table";	\
		done

dbSync:
	test -n "${target}"													||	(	\
		echo "Please set target"											&&	\
		exit 1																)
	test -n "${fromDB}"													||	(	\
		echo "Please set fromDB"											&&	\
		exit 1																)
	test -n "${fromDBHost}"												||	(	\
		echo "Please set fromDBHost"										&&	\
		exit 1																)
	test -n "${fromDBUser}"												||	(	\
		echo "Please set fromDBUser"										&&	\
		exit 1																)
	test -n "${fromDBPass}"												||	(	\
		echo "Please set fromDBPass"										&&	\
		exit 1																)
	test -n "${toDB}"													||	(	\
		echo "Please set toDB"												&&	\
		exit 1																)
	test -n "${toDBHost}"												||	(	\
		echo "Please set toDBHost"											&&	\
		exit 1																)
	test -n "${toDBUser}"												||	(	\
		echo "Please set toDBUser"											&&	\
		exit 1																)
	test -n "${toDBPass}"												||	(	\
		echo "Please set toDBPass"											&&	\
		exit 1																)
	test -n "${dbBackupFile}"											||	(	\
		echo "Please set dbBackupFile"										&&	\
		exit 1																)
	test -n "${dbBackupDir}"											||	(	\
		echo "Please set dbBackupDir"										&&	\
		exit 1																)
	echo ${target} for ${fromDB}@${fromDBHost} to ${toDB}@${toDBHost}

	export tables=`echo 'show tables'										|	\
		mysql -h${fromDBHost} -u${fromDBUser} -p${fromDBPass} ${fromDB}		|	\
		grep -v access_log | tail -n +2 | sort`								&&	\
	echo ${indent}Starting sync												&&	\
	(																			\
		echo 'DROP DATABASE IF EXISTS ${toDB};'								&&	\
		echo 'CREATE DATABASE IF NOT EXISTS ${toDB};'						&&	\
		echo 'use ${toDB};'													&&	\
		echo $$tables | xargs -n 10 mysqldump --single-transaction				\
			--skip-add-locks -h${fromDBHost} -u${fromDBUser} -p${fromDBPass}	\
			${dumpOpts} --quick ${fromDB} || true								\
	) ${verbosePipe} | tee ${dbBackupFile} | mysql -h${toDBHost}				\
		-u${toDBUser} -p${toDBPass} ${toDB}

# Sync the dev DB up with the production DB
prodDBSync:
	${make} logStart section=$@
	test -f $@															||	(	\
		make dbSync target=$@ fromDB=${dbName} fromDBHost=${prodDBServer}		\
			toDB=${MW_DB_NAME} toDBHost=${dbHost} fromDBUser=${rootDbUser}		\
			toDBUser=${rootDbUser} toDBPass=${rootDbPass}						\
			fromDBPass=${rootDbPass}										)
	${make} logStop section=$@
	${make} updatePhp
	${make} cleanupUsers
	${make} rebuildData

#
# DO NOT USE Sync prod DB up with dev DB
devDBSync:
	${make} logStart section=$@
	test -f $@															||	(	\
		make dbSync target=$@ toDB=${dbName} toDBHost=${prodDBServer}			\
			fromDB=${MW_DB_NAME} fromDBHost=${dbHost} toDBUser=${rootDbUser}	\
			fromDBUser=${rootDbUser} fromDBPass=${rootDbPass}					\
			toDBPass=${rootDbPass}											)
	${make} logStop section=$@


#
.PHONY: syncWithRemote
syncWithRemote:
	test -n "${REMOTE_HOST}"											||	(	\
		echo "Please set REMOTE_HOST"										&&	\
		exit 1															)
	test -n "${REMOTE_PATH}"											||	(	\
		echo "Please set REMOTE_PATH"										&&	\
		exit 1																)
	test -n "${timingFile}"												||	(	\
		echo "Please set timingFile"										&&	\
		exit 1																)

	${make} logStart section=$@
	rsync --exclude .envrc --links												\
		"--filter=:- ./.rsyncignore" --exclude `basename ${timingFile}`			\
		--delete -av ${REMOTE_HOST}:${REMOTE_PATH}/ .
	${make} logStop section=$@

.PHONY: dbVerify
dbVerify:
	echo Please set MW_DB_TYPE!
	exit 1

.PHONY: dbVerifypercona
dbVerifypercona: dbVerifymysql

.PHONY: dbVerifymysql
dbVerifymysql:
	${php} -r 'if ( !function_exists( "mysqli_connect" ) ) { echo "Please install mysql extension for php.\n"; exit(1);}'
