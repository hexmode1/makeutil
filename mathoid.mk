.PHONY: rmMathoid
rmMathoid: dockerImgID=${dockerMathoid}:${mathoidTag}
rmMathoid: dockerRM

${dockerDir}/mathoid-config.yaml:
	echo ${indent}extracting ${notdir $@}
	export id=`${dockerCmd} create ${dockerMathoid}:${mathoidTag}`			&&	\
	${dockerCmd} cp $$id:/node_modules/mathoid/config.yaml -				|	\
	tar -C ${dockerDir} -x													&&	\
	mv ${dockerDir}/config.yaml $@											&&	\
	${dockerCmd} rm -f $$id

# Deploy mathoid
.phony: mathoid
mathoid: docker ${dockerDir}/mathoid-config.yaml
	${dockerCmd} ps -f status=running										|	\
			grep -q ${dockerMathoid}:${mathoidTag}						||	(	\
		${dockerCmd} run -d -p 10044:10044 -e NODE_TLS_REJECT_UNAUTHORIZED=0	\
			--add-host=${svcName}:${svcIP} --restart always						\
			-v ${dockerDir}/mathoid-config.yaml:/node_modules/mathoid/config.yaml	\
			${dockerMathoid}:${mathoidTag}								)
