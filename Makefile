# Demonstration for how to use this repository

# Copyright (C) 2019  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# when trying to include makeutil/baseCofig.mk, Make will look for a
# rule to create the missing file.  Use this opportunity to bring in a
# few cohorts
include makeutil/baseConfig.mk
baseConfigGitRepo=https://phabricator.nichework.com/source/makefile-skeleton

.git:
	echo This is obviously not a git repository! Run 'git init .'
	exit 2

#
makeutil/%.mk: /usr/bin/git .git
	test -f $@																||	\
		git submodule add ${baseConfigGitRepo} makeutil
