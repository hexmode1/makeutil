# Rules to get composer installed correctly

# Copyright (C) 2019  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

mkfilePath := $(abspath $(lastword $(MAKEFILE_LIST)))

composerInstallerSigUrl=https://composer.github.io/installer.sig
composerInstallerUrl=https://getcomposer.org/installer

listJSONurl     := repo.packagist.org/packages/list.json
packageListJSON := ${assets}/${listJSONurl}

#
make ?= make -f ${mkfilePath}

#
curl ?= curl -f -L ${verboseCurl}

# Composer version to use
composerVer ?= 2

# Download composer and verify binary
composer: composerCert
	test -f $@																			||	(	\
		${make} composer.phar																&&	\
		mv composer.phar composer															)

#
.PHONY: fetchFromUrl
fetchFromUrl: /usr/bin/curl
	test -f ${file}																			||	\
		${curl} ${curlCAPath} -o ${file} ${url}
	touch ${file}

expected:
	${make} fetchFromUrl url=${composerInstallerSigUrl} file=$@

installer:
	${make} fetchFromUrl url=${composerInstallerUrl} file=$@

composer-setup.php: installer expected
	test -f composer																	||	(	\
		echo ${indent}Getting $@															&&	\
		echo `cat expected` ' installer' | sha384sum -c -									&&	\
		rm expected																			&&	\
		mv installer $@																		)

composer.phar: composer-setup.php
	test -f composer																	||	(	\
		${php} composer-setup.php --${composerVer} ${csNoSSL} ${composerQuiet}				&&	\
		rm composer-setup.php																)

${phpCaCert}: composer.phar
${phpCaCert}: composerCert

.PHONY: composerCert
composerCert: ${phpCaCert} ${proxySSL} composer.phar
	test -z "${proxySSL}" 																||	(	\
		rm -f ${phpCaCert}																	&&	\
		mkdir -p `dirname ${phpCaCert}`														&&	\
		${php} -d phar.readonly=0 -r "(new Phar('composer.phar'))								\
			->extractTo( '${phpCaCertDir}', '${phpCaCertFile}');"							&&	\
		cat "${proxySSL}" >> "${phpCaCert}"													)

# Download packagist's list.json
listJSON: ${packageListJSON}

#
.PHONY: ${packageListJSON}
${packageListJSON}:
	grep -q $(subst ${currentDir},,${packageListJSON}) ${currentDir}/.gitignore				||	\
		echo $(subst ${currentDir},,${packageListJSON}) >> ${currentDir}/.gitignore
	checkFile=${assets}/checkFile															&&	\
	touch -d yesterday $$checkFile															&&	\
	test -f ${packageListJSON} -a ${packageListJSON} -nt $$checkFile					||	(	\
		test -d `dirname $@` || mkdir -p `dirname $@`															&&	\
		cd ${assets}																		&&	\
		${curl} "https://${listJSONurl}" > $@													)
	rm -f $$checkFile
