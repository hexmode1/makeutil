# Take care of pkg installation

# Copyright (C) 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

OS=$(shell lsb_release -i | sed 's,^.*:\t,,')
OSMajorVer=$(shell lsb_release -r | sed 's,^.*:\t,,; s,\..*$,,;')
thisDir := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

#
default:
	false

#
updatePkg:
	echo in $@

updatePkgRedHatEnterpriseServer:
	${make} doCmd cmd='sudo yum update'

updatePkgDebian:
	echo in $@
	${make} doCmd cmd='sudo apt update'

# Install LAMP stack on remote host
lampPkgSet: updatePkg apachePhp
	test -z "${REMOTE_HOST}"											||	(	\
		${make} doRemote cmd="make $@"											)
	test -n "${REMOTE_HOST}"											||	(	\
		${make} $@${OS}														)

#
lampPkgSetDebian:
	${make} doCmd cmd="sh -c 'test -d /etc/apache2 || sudo apt install -y		\
		php-mysql php-curl php-gd php-intl php-mbstring php-xml php-zip			\
		libapache2-mod-php'"
	${make} doCmd cmd="sh -c 'test -d /var/lib/mysql || sudo apt install -y		\
		mariadb-server'"
	touch $@

#
lampPkgSetRedHatEnterpriseServer:
	echo No packages for RHEL.
	exit 10

apachePhp: updatePkg
	test -z "${REMOTE_HOST}"											||	(	\
		${make} doRemote cmd="make $@"										)
	test -n "${REMOTE_HOST}"											||	(	\
		${make} $@${OS}														)

apachePhpRedHatEnterpriseServer:
	${make} doCmd cmd="sudo yum install -y httpd php${PHP_VER}-fpm mod_ssl"
	${make} doCmd cmd="sudo systemctl enable httpd"
	${make} doCmd cmd="sudo systemctl enable php-fpm"
	${make} setupPhpFpm
	${make} doCmd cmd="sudo systemctl restart php-fpm"
	${make} doCmd cmd="sudo systemctl restart httpd"

setupPhpFpm:
	${make} cpFile file=php-fpm.conf dest=webConfD

${eval destLookup=$$(shell test -z "$$(1)" || jq -r .$${OS}.$$(1) $${thisDir}dest.json)}

cpFile:
	test -n "${file}"													||	(	\
		echo No file to copy.  Please set file!								&&	\
		exit 10																)
	test -n "${dest}"													||	(	\
		echo No dest key to copy to.  Please set dest!						&&	\
		exit 10																)
	test "$(call destLookup,${dest})" != "null"							||	(	\
		echo Dest key "(${dest})" does not exist in dest.json!				&&	\
		exit 10																)
	test -n "${REMOTE_HOST}"												||	\
		sudo cp ${thisDir}${file} "$(call destLookup,${dest})"
	test -z "${REMOTE_HOST}"												||	\
		${make} doCmd cmd="make cpFile file=${file} dest=${dest}"

doRemoteCp:
	test -n "${REMOTE_HOST}"											||	(	\
		echo No remote host available!										&&	\
		exit 1																)

php: updatePkg
	test -z "${REMOTE_HOST}"											||	(	\
		${make} doRemote cmd="make $@"											)
	test -n "${REMOTE_HOST}"											||	(	\
		${make} $@${OS}														)

phpDebian:
	${make} doCmd cmd="sudo apt install -y php-curl php-mbstring php-xml		\
		php-zip php-intl php-gd libapache2-mod-php"

phpRedHatEnterpriseServer:
	${make} doCmd cmd="sudo yum install -y 										\
		php${PHP_VER}-mbstring php${PHP_VER}-xml php${PHP_VER}-pecl-zip			\
		php${PHP_VER}-intl php${PHP_VER}-gd"

coreutils: updatePkg
	${make} doCmd cmd="sh -c 'test -x /usr/bin/tee || sudo apt install -y		\
		coreutils'"
	touch $@

etckeeper: updatePkg
	${make} doCmd cmd="sh -c 'test -x /usr/bin/etckeeper					||	\
		 sudo apt install -y etckeeper'"
	${make} doCmd cmd="sudo /usr/bin/etckeeper init"
	touch $@

pkgUpgradable:
	${make} doCmd cmd="sudo apt list --upgradable"

pkgUpgrade:
	${make} doCmd cmd="sudo apt upgrade -y"

/usr/bin/dig /usr/bin/nsupddate:
	apt install -y dnsutils

pv: /usr/bin/pv
/usr/bin/pv:
	apt install -y pv

jq: /usr/bin/jq
/usr/bin/jq:
	yum install -y jq

.PHONY: pandoc
pandoc:
	test -f /usr/bin/pandoc													||	\
		sudo yum install -y pandoc

.PHONY: xsltproc
xsltproc:
	test -f /usr/bin/xsltproc												||	\
		sudo yum install -y libxslt

.PHONY: fop
fop:
	test -f /usr/bin/fop													||	\
		sudo yum install -y fop

.PHONY: php-zip
php-zip:
	export zipInst="`${PHP} -i | awk '/^Registered PHP Streams/'`"			&&	\
	zipInst="`echo $$zipInst | sed 's,.*=> ,,; s/, /\n/g;' | grep ^zip$$`"	&&	\
	test -n "'$$zipInst'"													||	\
		sudo yum install -y php${PHP_VER}-php-pecl-zip

.PHONY: php-gd
php-gd:
	export gdInst="`${PHP} -i | awk '/^GD Support/' | sed 's,.*=> ,,'`"		&&	\
	test "enabled" = "$$gdInst"												||	\
		sudo yum install -y php${PHP_VER}-php-gd
