# Set up a LAMP server on the remote host

# Copyright (C) 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

currentDir := $(notdir $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))))

# Install the basic LAMP stack
initLamp: updatePkg lampPkgSet
	${make} doRemote cmd="sh -c 'sudo systemctl enable apache2'"
	${make} doRemote cmd="sh -c 'sudo systemctl enable mariadb'"
	${make} doRemote cmd="sh -c 'sudo systemctl start apache2'"
	${make} doRemote cmd="sh -c 'sudo systemctl start mariadb'"

	curl -s -I ${REMOTE_HOST} | grep -q ^.*200.OK						||	(	\
		echo Did not get "'200 OK'" from ${REMOTE_HOST}						&&	\
		exit 1																)
	touch $@

#
verifyRemotePath:
	test -n "${REMOTE_PATH}"											||	(	\
		echo Please set REMOTE_PATH!										&&	\
		exit 1																)
	test -n "${REMOTE_USER}"											||	(	\
		echo Please set REMOTE_USER!										&&	\
		exit 1																)
	${make} doRemote cmd="bash -c 'test -d ${REMOTE_PATH}/html				||	\
			mkdir -p ${REMOTE_PATH}/{html,logs}'"
	${make} doRemote cmd="bash -c 'test -w ${REMOTE_PATH}/html				||	\
			chown -R ${REMOTE_USER} ${REMOTE_PATH}/html'"
	touch $@

setupSite: initLamp verifyRemotePath coreutils
	(																			\
		echo "<VirtualHost *:80>"											&&	\
		echo "	ServerName ${REMOTE_HOST}"									&&	\
		echo "	DocumentRoot ${REMOTE_PATH}/html"							&&	\
		echo "	ErrorLog ${REMOTE_PATH}/logs/error.log"						&&	\
		echo "	CustomLog ${REMOTE_PATH}/logs/access.log combined"			&&	\
		echo "	<Directory ${REMOTE_PATH}/html>"							&&	\
		echo "		Options FollowSymlinks Indexes"							&&	\
		echo "		Require all granted"									&&	\
		echo "		AllowOverride All"										&&	\
		echo "	</Directory>"												&&	\
		echo "</VirtualHost>"													\
	) | ${make} doRemote														\
		cmd="sh -c 'test -f /etc/apache2/sites-available/${REMOTE_HOST}.conf ||	\
			tee /etc/apache2/sites-available/${REMOTE_HOST}.conf'"
	${make} doRemote															\
		cmd="sh -c 'test -L /etc/apache2/sites-enabled/${REMOTE_HOST}.conf	||	\
			a2ensite ${REMOTE_HOST}'"
	${make} doRemote															\
		cmd="sh -c 'test ! -L /etc/apache2/sites-enabled/${REMOTE_HOST}.conf ||	\
			sudo systemctl reload apache2									||	\
			( sudo systemctl status apache2 && false )'"
	touch $@
