.PHONY: rmElasticSearch
rmElasticSearch: dockerImgID=${dockerElastic}:${elasticTag}
rmElasticSearch: dockerRM

dockerDir ?= $(realpath docker)

${dockerDir}/elasticsearch:
	test ! -d $@ || mkdir -p $@
	chmod 1777 $@

${dockerDir}/elasticsearch.yml:
	echo ${indent}extracting ${notdir $@}
	export id=`${dockerCmd} create ${dockerElastic}:${elasticTag}`			&&	\
	${dockerCmd} cp $$id:/usr/share/elasticsearch/config/elasticsearch.yml ${dockerDir} &&	\
	${dockerCmd} rm -f $$id
	echo 'bootstrap.system_call_filter: false' | sudo tee -a elasticsearch.yml

# Deploy ElasticSearch
.PHONY: elasticSearch
elasticSearch: docker ${dockerDir}/elasticsearch.yml ${dockerDir}/elasticsearch
	${dockerCmd} ps -f status=running										|	\
			grep -q ${dockerElastic}:${elasticTag}						||	(	\
		${dockerCmd} run -d -p 9201:9200 -p 9300:9300							\
			--add-host=${svcDomain}:${svcIP} --restart always					\
			-e discovery.type=single-node -e NODE_TLS_REJECT_UNAUTHORIZED=0		\
			-v ${dockerDir}/elasticsearch:/usr/share/elasticsearch/data			\
			-v ${dockerDir}/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml	\
			${dockerElastic}:${elasticTag}									)
