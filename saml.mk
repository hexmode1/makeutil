# The branch version of SimpleSAMLphp to use
samlVer=simplesamlphp-1.15

#
samlRepo=https://github.com/simplesamlphp/simplesamlphp.git
saml: html composer
	test ! -d $@/metadata/metarefresh-ge						||	\
		sudo mv $@/metadata .
	${make} doClone loc=$@ name=SAML stem=$@ repo=${samlRepo}
	${make} maybeEnsureBranch branch=${samlVer} loc=$@ name=SAML stem=
	${make} maybePatch loc=$@ name=SimpleSamlPHP								\
		diffFile=html-saml-${samlVer}.diff
	${make} maybeUpdateSubmodules loc=$@
	test ! -d metadata/metarefresh-ge										||	\
		sudo mv metadata $(basename $@)/metadata
	cd $@ && ${php} ${topDir}/composer update --no-dev
