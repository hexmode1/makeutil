# Running commands

# Copyright (C) 2019, 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
git ?= git

#
doCmd:
	echo in $@
	test -n "${cmd}"													||	(	\
		echo No cmd to run.  Please set cmd!								&&	\
		exit 10																)
	test -z "${REMOTE_HOST}"											||	(	\
		${make} doRemote cmd="${cmd}"										)
	test -n "${REMOTE_HOST}"											||	(	\
		${cmd}																)

#
doRemote:
	test -n "${cmd}"													||	(	\
		echo You need to set cmd!											&&	\
		exit 1																)
	test -n "${REMOTE_HOST}"											||	(	\
		echo You need to set REMOTE_HOST!									&&	\
		exit 1																)
	test -n "${stdIn}"													||	(	\
		echo Running "${cmd}" on ${REMOTE_HOST}								&&	\
		echo ==============================================================	&&	\
		ssh ${REMOTE_HOST} ${cmd}											)
	test -z "${stdIn}"													||	(	\
		echo Running "${cmd}" on ${REMOTE_HOST}	with stdin from ${stdIn}	&&	\
		echo ==============================================================	&&	\
		pv ${stdIn} | ssh ${REMOTE_HOST} ${cmd}								)

ensureDirAndFile:
	test -n "${dir}"													||	(	\
		echo dir should be defined.											&&	\
		exit 1																)
	test -n "${file}"													||	(	\
		echo file should be defined.										&&	\
		exit 1																)

symlinkConf: ensureDirAndFile
	echo ${indent} $@ for ${file}
	test -L ${dir}/${file}												||	(	\
		rm -f ${dir}/${file}												&&	\
		ln -s ../conf/${file} ${dir}/${file}								)

verifyTimingFile:
	test -n "${timingFile}"												||	(	\
		echo																&&	\
		echo "***** No file to log timings to. Please set timingFile"		&&	\
		exit 1																)

logStart: verifyTimingFile
	test -n "${section}"												||	(	\
		echo																&&	\
		echo "***** No section to start log"								&&	\
		exit 1																)
	echo '**** Starting ${section}: ' `date --rfc-3339=seconds`				|	\
		tee -a ${timingFile}

logStop: verifyTimingFile
	test -n "${section}"												||	(	\
		echo																&&	\
		echo "***** No section to stop log"									&&	\
		exit 1																)
	echo '**** Finished ${section}: ' `date --rfc-3339=seconds`				|	\
		tee -a ${timingFile}

.PHONY: checkForCommand
checkForCommand:
	type ${cmd} > /dev/null 2>&1 || ( echo ${cmd} is not installed; exit 10 )

.PHONY: git
git: cmd=git
direnv: checkForCommand

.PHONY: direnv
direnv: cmd=direnv
direnv: checkForCommand

.PHONY: jq
jq: cmd=jq
jq: checkForCommand
