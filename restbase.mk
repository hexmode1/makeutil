.PHONY: rmRestbase
rmRestbase: dockerImgID=${dockerRestbase}:${restbaseTag}
rmRestbase: dockerRM

${dockerDir}/restbase-${SERVER_DOMAIN}.yaml:
	echo ${indent}extracting ${notdir $@}
	export id=`${dockerCmd} create ${dockerRestbase}:${restbaseTag}`		&&	\
	${dockerCmd} cp $$id:/restbase/projects/droidwiki.yaml -				| 	\
		tar -C ${dockerDir} -x												&&	\
	mv ${dockerDir}/droidwiki.yaml $@										&&	\
	${dockerCmd} rm -f $$id

${dockerDir}/restbase-${SERVER_DOMAIN}-sys.yaml:
	echo ${indent}extracting ${notdir $@}
	export id=`${dockerCmd} create ${dockerRestbase}:${restbaseTag}`		&&	\
	${dockerCmd} cp $$id:/restbase/projects/sys/droidwiki.yaml -			| 	\
		tar -C ${dockerDir} -x												&&	\
	mv ${dockerDir}/droidwiki.yaml $@										&&	\
	${dockerCmd} rm -f $$id

${dockerDir}/restbase-config.yaml: ${dockerDir}/restbase-${SERVER_DOMAIN}.yaml	\
		${dockerDir}/restbase-${SERVER_DOMAIN}-sys.yaml
	echo ${indent}extracting ${notdir $@}
	export id=`${dockerCmd} create ${dockerRestbase}:${restbaseTag}`		&&	\
	${dockerCmd} cp $$id:/restbase/config.yaml - | tar -C ${dockerDir} -x	&&	\
	mv ${dockerDir}/config.yaml $@											&&	\
	${dockerCmd} rm -f $$id

# Deploy restbase
.phony: restbase
restbase: docker ${dockerDir}/restbase-config.yaml
	${dockerCmd} ps -f status=running										|	\
			grep -q ${dockerRestbase}:${restbaseTag}					||	(	\
		${dockerCmd} run -d -p 7231:7231  -e NODE_TLS_REJECT_UNAUTHORIZED=0		\
			--add-host=${svcDomain}:${svcIP} --restart always					\
			-v ${dockerDir}/restbase-config.yaml:/restbase/config.yaml			\
			-v ${dockerDir}/restbase-${SERVER_DOMAIN}.yaml:/restbase/projects/${SERVER_DOMAIN}.yaml			\
			-v ${dockerDir}/restbase-${SERVER_DOMAIN}-sys.yaml:/restbase/projects/sys/${SERVER_DOMAIN}.yaml	\
			${dockerRestbase}:${restbaseTag}								)
