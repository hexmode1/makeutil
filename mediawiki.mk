# Set up MediaWiki on the remote host

# Copyright (C) 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
include $(if $(findstring ${currentDir}/composer.mk,$(MAKEFILE_LIST)),,${currentDir}/composer.mk)
include $(if $(findstring ${currentDir}/cmds.mk,$(MAKEFILE_LIST)),,${currentDir}/cmds.mk)
include $(if $(findstring ${currentDir}/pkg.mk,$(MAKEFILE_LIST)),,${currentDir}/pkg.mk)
include $(if $(findstring ${currentDir}/lamp.mk,$(MAKEFILE_LIST)),,${currentDir}/lamp.mk)

mwBranch			?= master
lsDir				?= conf/LocalSettings.d
compLocalJson		?= conf/composer.local.json
selectedExtensions	?=

# Site name for MediaWiki
MW_SITENAME ?= Sitename

# Where things are installed
REMOTE_HOST ?=

# Root of WMF repos
gerritHead ?= https://gerrit.wikimedia.org/r

# git repo to use for MW
mwCoreRepo ?= ${gerritHead}/mediawiki/core

# Checkout MediaWiki
.PHONY: checkoutMediaWiki
checkoutMediaWiki: verifyGit verifyMwInstallPath
	test -e mediawiki/.git 																	||	\
		git submodule add --depth=1 -b ${mwBranch} ${mwCoreRepo} mediawiki
	cd mediawiki && git submodule update --init
	test -d html || mkdir html
	test -L html/w || ln -s ../mediawiki html/w
	cd html && git add w
	grep -q $@ .gitignore																	||	\
		echo $@ >> .gitignore
	touch $@

#
mediawiki/vendor/autoload.php: mwComposerUpdate

# Run composer update in MW_INSTALL_PATH
.PHONY: mwComposerUpdate
mwComposerUpdate: verifyComposer verifyMwInstallPath
	test -f $@																				||	\
		${dockerCmd} exec ${siteName}-mw sh -c "php ../composer update --no-interaction			\
			 --no-dev --working-dir=w -vvv"
	grep -q $@ .gitignore																	||	\
		echo $@ >> .gitignore
	touch $@

initdb:
	mkdir initdb

dump ?= y

# Get a virgin MW dump to populate the Docker image
initdb/initial-dump.sql: initdb installMediaWiki
	test "${dump}" -eq "y" || ${make} dumpDB sqlDump=$@
	${make} prefixDump sqlDump=$@

.PHONY: prefixDump
prefixDump: verifySQLDump
	(																							\
		echo 'CREATE DATABASE ${MW_DB_NAME}; USE ${MW_DB_NAME};'							&&	\
		echo "CREATE USER ${MW_DB_USER}@'%' IDENTIFIED BY '${MW_DB_PASSWORD}';"				&&	\
		echo "GRANT ALL PRIVILEGES ON ${MW_DB_NAME}.* TO ${MW_DB_USER}@'%';"				&&	\
		pv ${sqlDump}																			\
	) | $(call sponge,${sqlDump})

# Run a dump
.PHONY: dumpDB
dumpDB: pv verifySQLDump
	test -n "${sqlDump}"																||	(	\
		echo You need to set sqlDump.														&&	\
		exit 1																				)
	dir=`dirname ${sqlDump}` && mkdir -p $$dir
	export running=`${dockerCmd} ps -f name=${siteName}-db										\
		--format='{{.ID}}' -f status=running`												&&	\
	test -n "$$running"															||	(			\
		echo ${dbContainer} is not running.											&&			\
		exit 1																		)	&&	(	\
	echo Dumping ${MW_DB_NAME} to ${sqlDump}												&&	\
	echo ==============================================================						&&	\
	${dockerCmd} exec $$running	mysqldump -h${MW_DB_SERVER} -uroot -p${MYSQL_ROOT_PASSWORD}		\
		${MW_DB_NAME} | pv > ${sqlDump}														)

# Populate local DB (usually test) with remote DB data remote
.PHONY: syncDB
syncDB:
	export sqlDump=wiki-dump-$(shell date +%Y-%m-%d-%M:%H).sql								&&	\
	${make} dumpDB sqlDump=$$sqlDump														&&	\
	${make} doRemote stdIn=$$sqlDump															\
		cmd='sudo mysql ${MW_DB_NAME}'

#
.PHONY: installMediaWiki
installMediaWiki: checkoutMediaWiki conf/LocalSettings.php mediawiki/vendor/autoload.php		\
		verifyWikiUserName verifyMWSitename verifyWikiUserPass verifyScriptPath 				\
		checkoutMediaWiki installExtensions mwComposerUpdate verifyRootPass
	rm -f mediawiki/LocalSettings.php
	${make} installPhp
	rm -f mediawiki/LocalSettings.php
	${make} symlinkConf dir=mediawiki file=LocalSettings.php

# Run install.php with the params
.PHONY: installPhp
installPhp:
	MW_INSTALL_PATH=`pwd`/mediawiki ${php} mediawiki/maintenance/install.php					\
		--pass=${WIKIUSER_PASS}	--dbprefix=${MW_DB_PREFIX}										\
		--dbtype=${MW_DB_TYPE} --dbname=${MW_DB_NAME} --dbuser=${MW_DB_USER}					\
		--dbpass=${MW_DB_PASSWORD} --dbserver=${MW_DB_SERVER} --scriptpath=${MW_SCRIPT_PATH}	\
		--installdbuser=root --installdbpass=${MYSQL_ROOT_PASSWORD}								\
		${MW_SITENAME} ${WIKIUSER_NAME}

# Install Mediawiki on remote host
deployMediaWiki: checkoutMediaWiki mwComposerUpdate installMediaWiki

#
install-skin-%: verifyLSDir
	export name=$(subst install-skin-,,$@)													&&	\
	test -d mediawiki/skins/$$name -a															\
		-f ${lsDir}/skins-$$name.php											||	(			\
		echo ${indent}installing skin $$name										&&			\
		${make} skin-$$name															)	&&	(	\
		echo ${indent}$$name is installed													)

#
install-ext-%: verifyLSDir
	export name=$(subst install-ext-,,$@)													&&	\
	test -d mediawiki/extensions/$$name -a														\
		-f ${lsDir}/extension-$$name.php										||	(			\
		echo ${indent}installing extension $$name									&&			\
		${make} extension-$$name													)	&&	(	\
		echo ${indent}$$name is installed													)

#
skin-%:
	${make} install-skin branch=${mwBranch} name=$(subst skin-,,$@)								\
		repo=$(or ${repo},${gerritHead}/mediawiki/skins/$(subst skin-,,$@))

#
remove-extension-%:
	echo ${indent}Removing old installation of $(subst remove-extension-,,$@)
	rm -rf .git/modules/conf/extensions/$(subst remove-extension-,,$@)							\
		conf/extensions/$(subst remove-extension-,,$@)											\
		mediawiki/extensions/$(subst remove-extension-,,$@)										\
		${lsDir}/$(subst remove-,,$@).php


#
redeploy-extension-%:
	${make} $(subst redeploy-,remove-,$@)
	${make} $(subst redeploy-,,$@)

#
extension-%:
	${make} install-extension branch=${mwBranch} name=$(subst extension-,,$@)	\
		repo=$(or ${repo},${gerritHead}/mediawiki/extensions/$(subst extension-,,$@))

#
.PHONY: spongeMergeInstallExtension
spongeMergeInstallExtension: install-extension verifyCompLocalJson
	export json="`jq -r -M . < ${compLocalJson}`"											&&	\
	export extCompPath="extensions/${name}/composer.json"									&&	\
	export exists="`echo $$json															|	jq	\
		\"contains({extra:{\\\"merge-plugin\\\":{include:										\
		[\\\"$$extCompPath\\\"]}}})\"`"														&&	\
	export requires="`echo $$json														|	jq	\
		\"contains({require:{\\\"${compID}\\\":\\\"\\\"}})\"`"								&&	\
	test "$$requires" = "false"															||	(	\
		echo "You asked for to merge the composer.json file for $$name but"						\
			"it is already in the composer requires."										&&	\
			exit 2																		)	&&	\
	test "$$exists" = "true"															||	(	\
		echo ${indent}Adding $$name to composer.local.json for merge.						&&	\
		echo $$json																			|	\
			jq ".extra[\"merge-plugin\"].include += [ \"$$extCompPath\" ]"					|	\
			$(call sponge,${compLocalJson})													)

#
.PHONY: install-extension
install-extension: stem=extension
install-extension: loc=conf/extensions/${name}
install-extension: conf/extensions enableStem

#
.PHONY: install-skin
install-skin: stem=skin
install-skin: loc=conf/skins/${name}
install-skin: conf/skins enableStem

#
.PHONY: legacy-extension
legacy-extension: verifyLSDir
	test -f ${lsDir}/${stem}-${name}.php										||	(	(		\
		echo '<?php'																	&&		\
		echo 'require_once "$$IP/extensions/${name}/${name}.php";'						)	>	\
			${lsDir}/${stem}-${name}.php											 )
	${make} install-extension branch=${mwBranch}												\
		name=${name}																			\
		repo=${gerritHead}/mediawiki/extensions/${name}

#
.PHONY: install-composer-extension
install-composer-extension: verifyCompLocalJson jq
	echo ${indent}Installing ${stem} "${name}"
	${make} install-extension tag=${tag} branch=${branch} name=${name}							\
		repo=${repo}

#
maybeUpdateLocalWithInclude: verifyCompLocalJson verifyName verifyStem verifyJsonFile
	jq -e .require < ${jsonFile} > /dev/null										&&	(		\
		echo ${indent}Adding include to composer.local.json for ${stem} "${name}"			&&	\
		jq ".extra[\"merge-plugin\"].include += [ \"${jsonFile}\" ]" < ${compLocalJson}		|	\
			$(call sponge,${compLocalJson})												)	||	\
	true

#
maybeUpdateLocalWithRequire: verifyCompLocalJson verifyJsonFile ${packageListJSON}				\
		verifyJsonName verifyCompID
	echo ${indent}Adding require $$name to composer.local.json
	jq '.["require"][${compID}]="$(if ${compVer},${compVer},*)"' < ${compLocalJson}			|	\
		$(call sponge,${compLocalJson})
	rm -f composerUpdate

#
maybeUpdateComposerLocalJson: ${packageListJSON} verifyName verifyStem
	export jsonFile=conf/${stem}s/${name}/composer.json										&&	\
	test ! -f $$jsonFile															||	(		\
		echo ${indent}Handling $$jsonFile													&&	\
		export jsonName=`jq .name < $$jsonFile`												&&	\
		test -n "$$jsonName"																&&	\
			jq -e ".packageNames | index($$jsonName)" < ${packageListJSON} > /dev/null		&&	\
				${make} maybeUpdateLocalWithRequire jsonFile=$$jsonFile compID=$$jsonName )	||	\
		${make} maybeUpdateLocalWithInclude jsonFile=$$jsonFile

#
.PHONY: enableStem
enableStem: loc=conf/${stem}s/${name}
enableStem: verifyName verifyStem updateRepo
	echo ${indent}$@ for ${name} ${stem}
	${make} LocalSettingsAddWf name=${name} stem=${stem}
	${make} maybeUpdateComposerLocalJson name=${name} stem=${stem}

#
.PHONY: ensureRemoteOrigin
ensureRemoteOrigin: url=$(or ${repo},${gerritHead}/mediawiki/extensions/${name})
ensureRemoteOrigin: verifyName verifyGit
	test -d mediawiki/${stem}s/${name}													||	(	\
		echo ${indent}$@ for ${url} at ${loc}												&&	\
		export origin=`cd ${loc} && ${git} remote get-url origin`							&&	\
		test -z "$$origin" -a "$$origin" = "${url}"									||	(		\
			echo ${indent}Updating url for the ${name} ${stem} \(${url}\).				&&		\
			cd ${loc} && ${git} remote set-url origin ${url}							)	)

#
.PHONY: updateRepo
updateRepo: verifyName checkoutAndLinkGit ensureRemoteOrigin verifyGit
	# don't do this if it is in MediaWiki repo already
	test -d mediawiki/${stem}s/${name}													||	(	\
		test ! -n "${tag}" -o ! -n "${branch}" || ( 											\
			echo -n Do not specify both tag \(${tag}\) and branch;								\
			echo " "\(${branch}\) for ${name}!					 							&&	\
			exit 1												 								\
		)														 							&&	\
		echo ${indent}Updating repo for ${name} in ${loc}		 							&&	\
		(														 								\
			cd ${loc} && ${git} reset --hard && ${git} status -s 							|	\
				awk '/^\?\?/ {print $2}' | xargs rm -rf;										\
			${git} fetch origin																	\
		)																					)
	${make} maybeEnsureBranch branch=${branch} stem=${stem} name=${name}						\
		loc=${loc}
	${make} maybeEnsureTag tag=${tag} stem=${stem} name=${name}									\
		loc=${loc}
	echo ${indent}Handling any submodules
	cd ${loc} && ${git} submodule update --init --recursive

	test ! -d mediawiki/${stem}s/${name}										||	\
		test -L ${loc}																	||		\
			ln -s ../../mediawiki/${stem}s/${name} ${loc}

	test -d ${loc} -o -L ${loc} 										||	\
		test -L mediawiki/${stem}s/${name}												||		\
			ln -s ../../conf/${stem}s/${name} mediawiki/${stem}s/${name}

	${make} maybePatch stem=${stem} name=${name} diffFile=${stem}-${name}-${tag}.diff
	${make} maybePatch stem=${stem} name=${name} diffFile=${stem}-${name}-${branch}.diff
	${make} maybePatch stem=${stem} name=${name} diffFile=${stem}-${name}.diff

#
.PHONY: maybePatch
maybePatch:
	test ! -f patches/${diffFile}														||	(	\
		echo ${indent}Patching ${name} with ${diffFile}										&&	\
		cd ${loc}																			&&	\
		git am --ignore-whitespace ${topDir}/patches/${diffFile}							)

#
.PHONY: checkoutAndLinkGit
checkoutAndLinkGit: verifyName verifyGit
	echo ${indent}Handling the ${name} ${stem}

	test -e mediawiki/${stem}s/${name}													||	(	\
		${make} doClone linkLoc=${linkLoc} loc=conf/${stem}s/${name}							\
			name=${name} stem=${stem}															\
			repo=$(or ${repo},${gerritHead}/mediawiki/extensions/${name})					&&	\
		${make} maybeUpdateSubmodules loc=conf/${stem}s/${name}								)

#
doClone:
	test ! -n "${DESTRUCTIVE}" -o ! -d ${loc}												||	\
		rm -rf ${loc}
	echo ${indent}Cloning ${name} into ${stem}s												&&	\
		${git} submodule add -f ${repo} ${loc}

#
maybeUpdateSubmodules: verifyGit
	# Add any submodules
	test -e ${loc}/.gitmodules															||	(	\
		cd ${loc} && ${git} ${gitSsl} submodule update --init								)

#
.PHONY: maybeEnsureBranch
maybeEnsureBranch: verifyGit
	test -z "${branch}"																	||	(	\
		echo ${indent}Ensuring ${name} ${stem} is on ${branch}								&&	\
		cd ${loc} && ${git} fetch															&&	\
		test $$(${git} branch | awk '/^\*/ {print $$2}') = "${branch}"						||	\
				${git} ${gitSsl} checkout ${branch}											)

#
.PHONY: maybeEnsureTag
maybeEnsureTag: verifyGit
	test -z "${tag}"																	||	(	\
		echo ${indent}Ensuring ${name} ${stem} is on the tag ${tag}							&&	\
		cd ${loc} && ${git} ${gitSsl} fetch													&&	\
			${git} checkout ${tag}															)

#
.PHONY: LocalSettingsAddWf
LocalSettingsAddWf: verifyName verifyLSDir conf/LocalSettings.php
	echo ${indent}$@ for ${stem} ${name}

	# Lucky us, PHP is case-insensitive for function names, so we
	# call wfLoadskin() and wfLoadextension()
	test -f ${lsDir}/${stem}-${name}.php												||	(	\
	(	echo '<?php'																		&&	\
		echo 'wfLoad${stem}( "${name}" );' ) > ${lsDir}/${stem}-${name}.php					)

#
.PHONY: redeployExtensions
redeployExtensions:
	export bail=''																			&&	\
	for ext in ${selectedExtensions}													;	do	\
		test -n "$$bail"															||	(		\
			${make} remove-extension-$$ext												&&		\
			${make} install-ext-$$ext || bail=$$ext										)	;	\
	done																					;	\
	test -n "$$bail"																&&	(		\
		echo "Failed on $$bail!"															&&	\
	 	exit 2																			)	||	\
		true
	grep -q $@ .gitignore																	||	\
		echo $@ >> .gitignore
	touch $@


#
.PHONY: installExtensions
installExtensions:
	export bail=''																			&&	\
	for ext in ${selectedExtensions}													;	do	\
		test -n "$$bail"																	||	\
			${make} install-ext-$$ext || bail=$$ext											;	\
	done																					;	\
	test -n "$$bail"																&&	(		\
		echo "Failed on $$bail!"															&&	\
	 	exit 2																			)	||	\
		true
	grep -q $@ .gitignore																	||	\
		echo $@ >> .gitignore
	touch $@

#
.PHONY: rebuildData
rebuildData: composerUpdate verifyMWINSTALL verifySMWjson
	${make} logStart section=$@
	${make} doMwCommand cmd="${php}																\
		${MW_INSTALL_PATH}/extensions/SemanticMediaWiki/maintenance/rebuildData.php				\
		${verboseUpdatePhp}"
	${make} logStop section=$@

#
.PHONY: cleanupUsers
cleanupUsers: composerUpdate verifySMWjson
	${make} logStart section=$@
	${make} doMwCommand cmd="${php} ${MW_INSTALL_PATH}/maintenance/cleanupUsersWithNoId.php		\
		-p noid ${verboseUpdatePhp}"
	${make} logStop section=$@

#
.PHONY: updatePhp
updatePhp: composerUpdate verifySMWjson
	${make} logStart section=$@
	${make} doMwCommand cmd="${php} ${MW_INSTALL_PATH}/maintenance/update.php --quick			\
		${verboseUpdatePhp}"
	${make} logStop section=$@

#
.PHONY: composerInstall
composerInstall: verifyComposer
	${make} logStart section=$@
	${make} doMwCommand cmd="${php} ${topDir}/composer install"
	${make} logStop section=$@

#
composerUpdate:
	${make} verifyComposer
	${make} logStart section=$@
	${make} doMwCommand cmd="${php} ../composer update --no-interaction ${verboseComposer}		\
		--working-dir=${MW_INSTALL_PATH}"
	${make} logStop section=$@
	grep -q $@ .gitignore																	||	\
		echo $@ >> .gitignore
	touch $@

#
conf/LocalSettings.php: conf verifyCompLocalJson
	test -f $@ || cp ${assets}/LocalSettings.php $@
	${make} symlinkConf dir=mediawiki file=LocalSettings.php

${SMW_CONF_FILE_DIR}/.smw.json: verifySMWConfFileDir
	${dockerCmd} exec ${siteName}-mw sh -c "echo '{}' | tee $@ > /dev/null"

conf ${lsDir}:
	test -d $@																			||	(	\
		echo ${indent}Creating $@...														&&	\
		mkdir -p $@																			)

conf/composer.local.json:
	test ! -L mediawiki/composer.local.json -a 													\
			-f mediawiki/composer.local.json												&&	\
		mv mediawiki/composer.local.json $@ || true
	${make} symlinkConf dir=mediawiki file=composer.local.json

conf/composer.lock: conf/composer.local.json
	test ! -L mediawiki/composer.lock -a -f mediawiki/composer.lock							&&	\
		mv mediawiki/composer.lock $@ || true
	${make} symlinkConf dir=mediawiki file=composer.lock
	test -f $@ || ${make} composerInstall

linkDir:
	for i in mediawiki/${dir}/*; do														 		\
		ext=`basename $$i`																	&&	\
		test -L $$i -o -f $$i -o -L ${target}/$$ext											||	\
			ln -s ../../$$i ${target};															\
	done

	for i in ${target}/*; do																	\
		ext=`basename $$i` &&																	\
		test -L $$i -o -f $$i -o -L mediawiki/${dir}/$$ext									||	\
			ln -s ../../$$i mediawiki/${dir};													\
	done

conf/extensions: conf
	test -d $@ || mkdir -p $@
	${make} linkDir dir=extensions target=$@

conf/skins: conf
	test -d $@ || mkdir -p $@
	${make} linkDir dir=extensions target=$@

conf/vendor: conf
	test -d $@ || mkdir -p $@

.PHONY: verifyLSDir
verifyLSDir:
	test -n "${lsDir}"																	||	(	\
		echo "Please set lsDir!"															;	\
		exit 1																					)
	${make} ${lsDir}

.PHONY: verifyMWINSTALL
verifyMWINSTALL:
	test -f ${MW_INSTALL_PATH}/maintenance/update.php									||	(	\
		echo Please set MW_INSTALL_PATH! MW_INSTALL_PATH is currently						&&	\
		echo ${MW_INSTALL_PATH} and maintenance/update.php is not there!					&&	\
		echo && exit 1																		)

.PHONY: verifySMWConfFileDir
verifySMWConfFileDir:
	test -n "${SMW_CONF_FILE_DIR}"														||	(	\
		echo Please set SMW_CONF_FILE_DIR.													;	\
		exit 1																				)
	${dockerCmd} exec ${siteName}-mw sh -c "mkdir -p ${SMW_CONF_FILE_DIR}"
	${dockerCmd} exec ${siteName}-mw sh -c "chmod 1777 ${SMW_CONF_FILE_DIR}"

.PHONY: verifySMWjson
verifySMWjson: ${SMW_CONF_FILE_DIR}/.smw.json
	# test `id -u ${WEB_USER}` -eq `stat -c %u ${SMW_CONF_FILE_DIR}` -a							\
	# 	`id -u ${WEB_USER}` -eq `stat -c %u $<`											||	(	\
	# 	echo ${indent}sudo chown ${WEB_USER} ${SMW_CONF_FILE_DIR} $<						&&	\
	# 	sudo chown ${WEB_USER} ${SMW_CONF_FILE_DIR} $<										)

.PHONY: verifyGit
verifyGit:
	test -n "${git}" || ( echo Please install git and set \$$\{git\}; exit 2 )
	${git} --help > /dev/null || ( echo Please ensure ${git} is git.; exit 2 )

.PHONY: verifyCompLocalJson
verifyCompLocalJson:
	test "${compLocalJson}" = "conf/composer.local.json"								||	(	\
		echo Please do not change \$$compLocalJson; exit 2									)
	test ! -e "${compLocalJson}" || test -f ${compLocalJson}							||	(	\
		echo ${compLocalJson} is not a file; exit 2											)
	${make} ${compLocalJson}
	export json="`cat ${compLocalJson} 2> /dev/null											|	\
		 jq -r -M . 2>/dev/null`" || true													&&	\
	test -n "$$json"																		||	\
		json='{}'																			&&	\
	echo $$json | jq '.require += { "vlucas/phpdotenv": "~4" }'								|	\
		$(call sponge,${compLocalJson})

.PHONY: verifyWikiUserName
verifyWikiUserName:
	test -n "${WIKIUSER_NAME}"															||	(	\
	echo "Please set WIKIUSER_NAME!"														;	\
	exit 1																					)

.PHONY: verifyWikiUserPass
verifyWikiUserPass:
	test -n "${WIKIUSER_PASS}"															||	(	\
	echo "Please set WIKIUSER_PASS!"														;	\
	exit 1																					)

.PHONY: verifyMWSitename
verifyMWSitename:
	test -n "${MW_SITENAME}"															||	(	\
	echo "Please set MW_SITENAME!"															;	\
	exit 1																					)

.PHONY: verifyScriptPath
verifyScriptPath:
	test -n "${MW_SCRIPT_PATH}"															||	(	\
	echo "Please set MW_SCRIPT_PATH!"														;	\
	exit 1																					)

.PHONY: verifyComposer
verifyComposer:
	test -f composer -a -x composer															||	\
		${make} composer

.PHONY: verifyMwInstallPath
verifyMwInstallPath:
	test -n "${MW_INSTALL_PATH}"														||	(	\
		echo You need to set MW_INSTALL_PATH.												&&	\
		exit 1																				)

.PHONY: verifyRootPass
verifyRootPass:
	test -n "${MYSQL_ROOT_PASSWORD}"													||	(	\
		echo You need to set MYSQL_ROOT_PASSWORD.											&&	\
		exit 1																				)

.PHONY: verifyStem
verifyStem:
	test -n "${stem}"																	||	(	\
		echo "Choose a stem!";																	\
		exit 1																				)

.PHONY: verifyName
verifyName: verifyStem
	test -n "${name}"																	||	(	\
		echo A name must be specified for this ${stem}!;										\
		exit 1																				)

.PHONY: verifySQLDump
verifySQLDump:
	test -n "${sqlDump}"																||	(	\
		echo Please set the envvar sqlDump!													&&	\
		exit 1																				)

.PHONY: verifyJsonFile
verifyJsonFile:
	test -n "${jsonFile}"																||	(	\
		echo Please set the envvar jsonFile!												&&	\
		exit 1																				)

.PHONY: verifyJsonName
verifyJsonName:
	test -n "${jsonName}"																||	(	\
		echo Please set the envvar jsonName!												&&	\
		exit 1																				)

.PHONY: verifyCompID
verifyCompID:
	test -n "${compID}"																	||	(	\
		echo Please set the envvar compID for ${jsonName}!									&&	\
		exit 1																				)

.PHONY: verifyCompVer
verifyCompVer:
	test -n "${compVer}"																||	(	\
		echo Please set the envvar compVer for ${jsonName}!									&&	\
		exit 1																				)

.PHONY: verifyCmd
verifyCmd:
	test -n "${cmd}"																	||	(	\
		echo Please provide a command to run,												&&	\
		exit 1																				)
