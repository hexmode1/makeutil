
${luasandboxSO}: ${phpDevDep} ${luaDevDep} /usr/bin/lua
	${make} doClone name=luasandbox stem=source loc=luasandbox					\
		repo=${gerritHead}/mediawiki/php/luasandbox.git
	${make} maybeUpdateSubmodules loc=luasandbox
	cd luasandbox															&&	\
	${phpize}																&&	\
	./configure --with-php-config=${phpconfig}								&&	\
	make																	&&	\
	sudo make install

${luasandboxIni}:
	echo 'extension=luasandbox' | sudo tee $@ > /dev/null

.PHONY: verifyLuaVars
verifyLuaVars:
	test -n "${luasandboxSO}"											||	(	\
		echo "Please set luasandboxSO"										&&	\
		exit 1																)
	test -n "${luasandboxIni}"											||	(	\
		echo "Please set luasandboxIni"										&&	\
		exit 1																)

.PHONY: rmLuasandbox
rmLuasandbox: verifyLuaVars
	test ! -e "${luasandboxSO}" -a ! -e "${luasandboxIni}"				||	(	\
		sudo rm ${luasandboxSO} ${luasandboxIni}							&&	\
		sudo /etc/init.d/php${PHP_VER}-php-fpm restart						)

.PHONY: luasandbox
luasandbox: verifyLuaVars ${luasandboxSO} ${luasandboxIni}
	${PHP} -i | grep -i ^luasandbox > /dev/null							||	(	\
		echo "Installation of luasandbox didn't work!"						&&	\
		exit 1																)
	sudo /etc/init.d/php${PHP_VER}-php-fpm restart
