# Rules to update DNS via nsupdate

# Copyright (C) 2019, 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

currentDir := $(notdir $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))))
include $(if $(findstring ${currentDir}/pkg.mk,$(MAKEFILE_LIST)),,${currentDir}/pkg.mk)

# DNS server to update
dnsserver ?=

# List of all DNS servers
allDNSServers ?=

# Keyfile to use
keyfile ?= K${domain}.private

# DNS name to update
name ?=

# IP address to use
ip ?=

# Time to live
ttl ?= 3600

# Domain being updated
domain = $(shell echo ${name} | sed 's,.*\(\.\([^.]\+\.[^.]\+\)\)\.*$$,\2,')

#
verifyIP:
	test -n "${ip}"														||	(	\
		echo Please set ip!													&&	\
		exit 1																)

#
verifyDomain:
	test -n "${domain}"													||	(	\
		echo Could not determine domain. Please set domain!					&&	\
		exit 1																)
	test "${domain}" != "${name}"										||	(	\
		echo Problem parsing domain from name. Please set domain!			&&	\
		exit 1																)

#
verifyKeyfile:
	test -n "${keyfile}"												||	(	\
		echo No keyfile. Please set keyfile!								&&	\
		exit 1																)
	test -f "${keyfile}"												||	(	\
		echo "Keyfile (${keyfile}) does not exist!"							&&	\
		exit 1																)

# Add host with IP
addHost: verifyName verifyIP verifyDomain verifyKeyfile ${NSUPDATE}
	printf "server %s\nupdate add %s %d in A %s\nsend\n" "${dnsserver}"			\
		"${name}" "${ttl}" "${ip}" | ${NSUPDATE} -k ${keyfile}
	${make} checkDNSUpdate ip=${ip} name=${name}

# Check a record across all servers
checkDNSUpdate: verifyName verifyIP
	for server in ${allDNSServers}; do										\
		${make} checkAddr ip=${ip} name=${name} dnsserver=$$server		||	\
			exit 10														;	\
	done

# Check host has IP
checkAddr: verifyName verifyIP ${DIG}
	echo -n ${indent}Checking $$server for A record of ${name} on ${dnsserver}...
	${DIG} ${name} @${dnsserver} A | grep -q ^${name}.*IN.*A.*${ip}		||	(	\
		echo " FAIL!"														&&	\
		echo ${name} is not set to ${ip} on ${dnsserver}!					&&	\
		exit 1																)
	echo " OK"
