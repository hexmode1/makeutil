<?php
# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$getSetting = function ( $key, $default = null, $splitOn = null ) {
	static $env;
	if ( !$env ) {
		$dotenv = Dotenv\Dotenv::createMutable( dirname( __DIR__ ), ".envrc" );
		$dotenv->load();

		$env = $_ENV;
	}

	$ret = $default;
	if ( isset( $env[$key] ) ) {
		$ret = $env[$key];
	}

	if ( $ret === null ) {
		echo "Please set the envvar $key.\n";
		exit( 10 );
	}

	if ( $splitOn !== null ) {
		$ret = explode( $splitOn, $ret );
	}
	return $ret;
};

$wgShowExceptionDetails = true;
$wgDebugDumpSql = true;
error_reporting( -1 );
ini_set( 'display_startup_errors', 1 );
ini_set( 'display_errors', 1 );
if ( substr( strtoupper( $getSetting( "DEBUG", "N" ) ), 0, 1 ) === "N" ) {
	$wgShowExceptionDetails = false;
	$wgDebugDumpSql = false;
	error_reporting( 0 );
	ini_set( 'display_startup_errors', 0 );
	ini_set( 'display_errors', 0 );
}

		$wgSitename = $getSetting( "MW_SITENAME" );
	  $wgScriptPath = $getSetting( "MW_SCRIPT_PATH", "/w" );
$wgResourceBasePath = $getSetting( "MW_RESOURCE_BASE_PATH", $wgScriptPath );
	 $wgArticlePath = $getSetting( "MW_ARTICLE_PATH", '/wiki/$1' );
		  $wgServer = $getSetting( "MW_SERVER" );
			$wgLogo = $getSetting( "MW_LOGO", "{$wgScriptPath}/resources/assets/wiki.png" );
$wgEmergencyContact = $getSetting( "MW_EMERGENCY_CONTACT" );
  $wgPasswordSender = $getSetting( "MW_PASSWORD_SENDER", $wgEmergencyContact );
	 $wgCachePrefix = $getSetting( 'MW_CACHE_PREFIX', false );
		  $wgDBtype = $getSetting( "MW_DB_TYPE", "mysql" );
		  $wgDBPort = $getSetting( "MW_DB_PORT", "3306" );
		$wgDBserver = $getSetting( "MW_DB_SERVER", "localhost" )
					. ( $wgDBtype === "mysql" ? ":$wgDBPort" : "" );
		  $wgDBname = $getSetting( "MW_DB_NAME" );
		  $wgDBuser = $getSetting( "MW_DB_USER" );
	  $wgDBpassword = $getSetting( "MW_DB_PASSWORD" );
		$wgDBprefix = $getSetting( "MW_DB_PREFIX", "" );
	$wgCookiePrefix = $getSetting( "COOKIE_PREFIX", false );
	$wgCookieDomain = $getSetting( "COOKIE_DOMAIN", "" );
  $wgDBTableOptions = $getSetting( "MW_DB_TABLE_OPTIONS", "ENGINE=InnoDB, DEFAULT CHARSET=binary" );
		   $_mcPort = $getSetting( "MEMC_PORT", "11211" );
$wgMemCachedServers = $getSetting( "MW_MEM_CACHED_SERVERS", "localhost:$_mcPort", "|" );
	 $wgShellLocale = $getSetting( "MW_SHELL_LOCALE", "C.UTF-8" );
	$wgLanguageCode = $getSetting( "MW_LANGUAGE_CODE", "en" );
			$cfgDir = $getSetting( "WIKI_CFG_DIR", "/etc/wiki" );
  $wgCacheDirectory = $getSetting( "MW_CACHE_DIRECTORY", false );
			$logDir = $getSetting( "MW_LOG_DIR" );
   $wgMetaNamespace = $getSetting(
	   "MW_META_NAMESPACE", str_replace( " ", "_", ucfirst( $wgSitename ) )
   );

$wgExtensionDirectory = __DIR__ . "/extensions";
$wgStyleDirectory = __DIR__ . "/skins";
$wgSessionCacheType = $wgMessageCacheType = $wgParserCacheType
					= $wgMainCacheType = $wgSessionCacheType = CACHE_MEMCACHED;

$wgSessionsInObjectCache = true;
$wgMemCachedPersistent = true;

$wgUsePathInfo = true;

$localSettingsDir = __DIR__ . "/LocalSettings.d";
if ( is_dir( $localSettingsDir ) ) {
	$dir = new DirectoryIterator( $localSettingsDir );
	$order = [];
	foreach ( $dir as $file ) {
		$name = $file->getPathname();
		if (
			!$file->isDot() && $file->isReadable()
			&& substr( $name, -4 ) === ".php"
		) {
			$order[] = $name;
		}
	}
	sort( $order );

	foreach ( $order as $file ) {
		require $file;
	}
}
unset( $localSettingsDir, $_mcPort, $cfgDir, $logDir );
if ( !isset( $_SERVER['SERVER_NAME'] ) ) {
	$_SERVER['SERVER_NAME'] = "unknown!";
}
