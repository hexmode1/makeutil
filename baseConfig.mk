# This file is included first in our Makefiles to set some default behavior

# Copyright (C) 2019  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# This ensures that help is default target and is shown by default
#
helpstub: help
currentDir := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
topDir := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))
assets := ${currentDir}/assets

#
#
verbosePipe=| pv | tee ${DUMPDIR}dump.sql
verboseRsync=-v --progress
verboseCurl=
verbosePhpunit=--verbose
verboseComposer=-vvv
verboseImport=--report

ifndef VERBOSE
.SILENT: # Don't print commands
verbosePhp=-d error_reporting=30711
verboseComposer=--quiet
verboseCurl=-s
verboseMaint=-q
verbosePipe=
verboseRsync=
verboseMd5sum=--quiet --status
verbosePhpunit=
verboseComposer=
verboseImport=--quiet
endif

curlCAPath=$(if ${proxySSL},--capath ${proxySSL},)
csNoSSL=$(if ${proxySSL},--cafile=${proxySSL},)
ifdef NOSSL
csNoSSL=--disable-tls
curlCAPath=
sslGit=-c http.sslVerify=false
sslCurl=-k
sslPhp=$(shell test ! -f ${phpCaCert} || echo "-d openssl.cafile=${phpCaCert}")
sslCs=--disable-tls
phpCaCertDir=${topDir}/cacert
phpCaCertFile=vendor/composer/ca-bundle/res/cacert.pem
phpCaCert=${phpCaCertDir}/${phpCaCertFile}
endif

md5sum=md5sum -c --ignore-missing ${verboseMd5sum}
phpOpts=${verbosePhp} ${sslPhp}
#
PHP ?= You-Need-To-Set-PHP-EnvVar
php=${PHP} ${phpOpts}
curl=curl -L ${verboseCurl} ${sslCurl}
make=${MAKE} -f $(abspath $(firstword $(MAKEFILE_LIST))) indent="${indent}\> " VERBOSE=${VERBOSE}
mysql=mysql -u${dbUser} -p${dbPass} -h ${dbHost}
appendToFile=dd oflag=append conv=notrunc status=none of=
configurationMk=$(dir $(abspath $(firstword $(MAKEFILE_LIST))))configuration.m*k

# Turn off SSL checks -- !!INSECURE!!
NOSSL ?=
export NOSSL

# Print out every command
VERBOSE ?=
export VERBOSE

sponge = (																						\
		export sponge=`mktemp ./sponge.XXXX`												&&	\
		cat > $$sponge																		&&	\
		mv $$sponge $(1)																	&&	\
		chmod a+r $(1)																			\
	)

# Makefile help based on
# https://github.com/ianstormtaylor/makefile-help

.PHONY: help
# Show this help prompt
help:
	@ echo
	@ echo '  Usage:'
	@ echo ''
	@ echo '    make <target> [flags...]'
	@ echo ''
	@ echo '  Targets:'
	@ echo ''
	@ awk '/^#/{ comment = substr($$0,3) } comment							\
		 && /^[a-zA-Z][a-zA-Z0-9_-]+ ?: *[^=]*$$/ {							\
			 print "   ", $$1, comment										\
		}' $(MAKEFILE_LIST) | column -t -s ':' | sort
	@ echo ''
	@ echo '  Flags: (current value in parenthesis)'
	@ echo ''
	@ awk '/^#/{ comment = substr($$0,3) } comment							\
		 && /^[a-zA-Z][a-zA-Z0-9_-]+ ?\?= *([^=]*.*)$$/ {					\
			if ( ENVIRON[$$1] ) {											\
				val=ENVIRON[$$1];											\
			} else {														\
				val=$$3;													\
			}																\
			print "   ", $$1, $$2, comment, "(" val ")"						\
		}' $(MAKEFILE_LIST) | column -t -s '?=' | sort
	@ echo ''

# Show more targets and flags
morehelp:
	@ echo
	@ echo '  More Targets:'
	@ echo ''
	@ awk '	/^#$$/ { if( lastline == comment ) { more=1 } }					\
			/^#$$/ { if( lastline != comment && more == 1 ) { more=0 } }	\
			/^[a-zA-Z][a-zA-Z0-9_-]+ ?: *[^=]*$$/ {							\
				if ( more == 1 && prevlastline==comment) {					\
					print "   ", $$1, comment								\
				}															\
			}																\
			/^# / { comment = substr($$0,3) }								\
				{ prevlastline = lastline; lastline = substr($$0,3) } '		\
			$(MAKEFILE_LIST) | column -t -s ':' | sort
	@ echo ''
	@ echo '  More Flags:'
	@ echo ''
	@ awk '	/^#$$/ { if( lastline == comment ) { more=1 } }					\
			/^#$$/ { if( lastline != comment && more == 1 ) { more=0 } }	\
			/^[a-zA-Z][a-zA-Z0-9_-]+ ?\?= *([^=]*.*)$$/ {					\
				if ( more == 1 && prevlastline==comment) {					\
					print "   ", $$1, $$2, comment, 						\
						"(" ENVIRON[$$1] ")"								\
				}															\
			}																\
			/^# / { comment = substr($$0,3) }								\
				{ prevlastline = lastline; lastline = substr($$0,3) } '		\
			$(MAKEFILE_LIST) | column -t -s '?=' | sort
	@ echo ''

# Show how to write help for make files
#
helphelp:
	@ echo
	@ echo 'To write a rule or flag that you want shown in the default help,'
	@ echo 'simply add a single comment to the line just before the target.'
	@ echo 'If you want something to show up when the user types "make morehelp",'
	@ echo 'Put an empty comment after the comment that you want to appear.'
	@ echo
	@ echo 'Example:'
	@ echo
	@ echo '	# Flag comment example'
	@ echo '	flag ?= example'
	@ echo
	@ echo '	# More help flag comment example'
	@ echo '	#'
	@ echo '	anotherFlag ?= exampleFlag'
	@ echo
	@ echo '	# Target comment exmple'
	@ echo '	target: minorTarget'
	@ echo '  		echo flag is $${flag}'
	@ echo
	@ echo '	# A minor target'
	@ echo '	#'
	@ echo '	minorTarget:'
	@ echo '  		echo anotherFlag is $${anotherFlag}'

makefileGetGit: gitBase=https://phabricator.nichework.com/source
makefileGetGit: treeish=master
makefileGetGit: repo=makefile-skeleton
makefileGetGit: browse=browse
makefileGetGit: param=?view=raw
makefileGetGit: dest=${pathspec}
makefileGetGit: getGitFile

wmfGetGit: gitBase=https://gerrit.wikimedia.org/r/plugins/gitiles
wmfGetGit: browse=+
wmfGetGit: param=?format=TEXT
wmfGetGit: getGitFileCheck
	test -f "${dest}"															||	(	\
		file=`mktemp`																&&	\
		${curl} ${gitBase}/${repo}/${browse}/${treeish}/${pathspec}${param} > $$file &&	\
		base64 --decode < $$file > ${dest}											&&	\
		rm $$file																	)

getGitFileCheck:
	test -n "${gitBase}"														||	(	\
		echo Please define gitBase!													&&	\
		exit 1																			)
	test -n "${dest}"															||	(	\
		echo Please define dest!													&&	\
		exit 1																			)
	test -n "${repo}"															||	(	\
		echo Please define repo!													&&	\
		exit 1																			)
	test -n "${treeish}"														||	(	\
		echo Please define treeish!													&&	\
		exit 1																			)
	test -n "${pathspec}"														||	(	\
		echo Please define pathspec!												&&	\
		exit 1																			)

getGitFile: getGitFileCheck
	test -f "${dest}"																||	\
		${curl} ${gitBase}/${repo}/${browse}/${treeish}/${pathspec}${param} > ${dest}
