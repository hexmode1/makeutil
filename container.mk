# Podman Cmds

# Copyright (C) 2020  NicheWork, LLC

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
MW_SITENAME ?= "Site Name"

# php version with the dot
phpDotVer ?= 7.4

# php base container to use
phpBaseContainer ?= docker.io/library/php:7.4-apache

# Directory that holds the DB files for the container
dbStore ?= ./db

# Command to run containers
dockerCmd ?= podman

#
siteName	?= $(shell echo ${MW_SITENAME} | sed 's,[^[:alnum:]_.],-,g')
HOST_IP		?= $(shell ip route get 1 | awk '{print $$7;exit}')
MW_DOMAIN	?= none
MW_DBG_PORT ?= 9003
DB_PORT		?= 3306
MW_PORT		?= 80
MEMC_PORT	?= 11211
phpMWCont	?= mediawiki
rawPhpCont	?= raw-php
webUser		?= www-data
containRepo	?= localhost
dbContainer	?= docker.io/library/percona
memcCont	?= docker.io/library/memcached:latest
dbTag		?= latest
zipPhpVer	?= 1.19.0
memcPhpVer	?= 3.1.5
assets      ?= ${currentDir}/assets

${eval getContainers=$$(shell test -z "$$(1)"												||	\
	buildah containers -f ancestor=$$(1) --format '{{.ContainerID}}')}

${dbStore}:
	mkdir ${dbStore}
	chmod 1777 ${dbStore}

# Perform a command within the MediaWiki environment
.PHONY: doMwCommand
doMwCommand: verifyCmd
	${dockerCmd} exec -ii ${siteName}-mw sudo -u ${webUser} HOST_IP=${HOST_IP} sh -c "${cmd}"

# Perform a command within the Database environment
.PHONY: doDbCommand
doDbCommand: verifyCmd verifyStdIn
	test -z "${stdIn}"																		||	\
		cat ${stdIn} | ${dockerCmd} exec -i ${siteName}-db sh -c "${cmd}"
	test -n "${stdIn}"																		||	\
		${dockerCmd} exec -it ${siteName}-db sh -c "${cmd}"

checkDB='echo show create table ${MW_DB_PREFIX}watchlist | ${dockerCmd} exec -i					\
		 ${siteName}-db sh -c "mysql -p${MYSQL_ROOT_PASSWORD} ${MW_DB_NAME}" > /dev/null 2>&1'

#
.PHONY: waitForDB
waitForDB:
	echo -n "waiting for db: "
	until sh -c $(call checkDB); do																\
		echo -n .																			&&	\
		sleep 10																			;	\
	done
	echo done.

# Stop this MediaWiki site
.PHONY: startSite
startSite: dbServer mwServer memcServer

# Start this MediaWiki site
.PHONY: stopSite
stopSite: rmDbServer rmMwServer rmMemcServer

#
.PHONY: stopContainer
stopContainer:
	export running=$(shell ${dockerCmd} ps --format='{{.ID}}'									\
		-f status=running -f name=${name})													&&	\
	test -z "$$running"																	&&		\
		echo ${indent}${name} is not running.											||	(	\
		echo ${indent}Stopping ${name}...													&&	\
		${dockerCmd} rm -f $$running														)

#
.PHONY: dbServer
dbServer: ${dbStore} initdb
	${dockerCmd} run -d -p "${DB_PORT}:3306" -e MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}		\
		-v ${dbStore}:/var/lib/mysql -v ./initdb:/docker-entrypoint-initdb.d					\
		--name ${siteName}-db ${dbContainer}:${dbTag}
#
.PHONY: rmDbServer
rmDbServer:
	${make} stopContainer name=${siteName}-db

#
.PHONY: mwServer
mwServer: buildPhpMWContainer
	${dockerCmd} run -d -p "${MW_PORT}:80" -p "${MW_DBG_PORT}:9003" -e HOST_IP=${HOST_IP}		\
		-v `pwd`:/var/www -v ${HOME}/.composer:/root/.composer --name ${siteName}-mw			\
		-e VIRTUAL_HOST=${MW_DOMAIN} localhost/${phpMWCont}

#
.PHONY: rmMwServer
rmMwServer: buildPhpMWContainer
	${make} stopContainer name=${siteName}-mw

#
.PHONY: memcServer
memcServer:
	${dockerCmd} run -d -p "${MEMC_PORT}:11211" --name ${siteName}-memc ${memcCont}

#
.PHONY: rmMemcServer
rmMemcServer:
	${make} stopContainer name=${siteName}-memc

#
getRawPhpMWContainer: /usr/bin/buildah
	export id=$(shell buildah containers --filter 'name=${rawPhpCont}'							\
					--format "{{.ContainerID}}")											&&	\
	test -n "$$id"																		||	(	\
		buildah from --name "${rawPhpCont}" docker.io/library/php:${PHP_DOT_VER}-apache		&&	\
		buildah commit ${rawPhpCont} ${rawPhpCont}											)
	echo $@ complete.

rmPhpMWContainer:
	buildah rm ${phpMWCont}
	podman rmi -f ${phpMWCont}

#
.PHONY: buildPhpMWContainer
buildPhpMWContainer: getRawPhpMWContainer
	export id=$(shell buildah images --json													|	\
		jq -r 'map( select( any(																\
			.names[]?; contains( "${phpMWCont}:latest" )							\
			) ) | .id )[]')																	&&	\
	test -n "$$id"																			||	\
		buildah bud --pull-never --layers=true -f ${assets}/Containerfile -t ${phpMWCont}
	echo $@ complete.

#
.PHONY: verifyContainer
verifyContainer:
	test -n "${container}"																||	(	\
		echo "Please set container!"														&&	\
		exit 10																				)

#
.PHONY: verifyStdIn
verifyStdIn:
	test -z "${stdIn}" -o -f "${stdIn}"													||	(	\
		echo stdIn "(${stdIn})" is not a usable file!										&&	\
		exit 10																				)
